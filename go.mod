module gitlab.com/moussaelianarsen/acorn

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/go-git/go-git/v5 v5.1.0
	github.com/logrusorgru/aurora v2.0.3+incompatible
	github.com/rs/zerolog v1.20.0
)
