package main

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	. "github.com/logrusorgru/aurora"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

const acornDir = "/etc/acorn/"
const dlDir = acornDir + "pkgs/"
const repoDir = acornDir + "repos/"

func main() {
	// Use ConsoleWriter logger
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	// Get command line arguments
	args := os.Args
	// Remove executable path from command line arguments
	args = args[1:]
	// Print usage and exit if no command is provided
	if len(args) == 0 {
		log.Error().Msg("Command not provided")
		mainUsage()
		os.Exit(1)
	}
	// Determine command
	switch args[0] {
	case "install", "in":
		var cloneDir string
		// Print usage and exit if no package is provided
		if len(args) == 1 {
			log.Error().Msg("Package not provided")
			installUsage()
			os.Exit(1)
		}
		// Download specified packages
		for _, e := range args[1:] {
			// Log download event
			log.Info().Msg("Downloading " + e)
			if strings.Contains(e, "://") {
				// Clone git repo
				_, cloneDir = GitClone(e, dlDir)
			} else {
				log.Info().Msg("Fetching newest repolist")
				// Make repoDir if it doesn't exist
				err := os.MkdirAll(repoDir, 0755)
				if err != nil { log.Fatal().Err(err).Msg("Error creating repo directory") }
				// Create or open repolist.toml
				outFile, err := os.Create(repoDir + "repolist.toml")
				if err != nil { log.Fatal().Err(err).Msg("Error creating repolist file") }
				defer outFile.Close()
				// Get new repolist
				res, err := http.Get("https://gitea.arsenm.dev/acorn/repolist/raw/branch/master/repolist.toml")
				if err != nil { log.Warn().Err(err).Msg("Error fetching repolist") }
				defer res.Body.Close()
				// Clear contents of repolist
				outFile.Truncate(0)
				outFile.Seek(0,0)
				// Place new repolist in file
				io.Copy(outFile, res.Body)
				outFile.Sync()
				// Create data storage variable for repolist
				var repoListData map[string]interface{}
				// Decode repolist
				_, err = toml.DecodeFile(repoDir + "repolist.toml", &repoListData)
				if err != nil { log.Fatal().Err(err).Msg("Error decoding repolist") }
				// Walk repoDir to find external repolists
				err = filepath.Walk(repoDir, func(path string, info os.FileInfo, err error) error {
					if err != nil { return err }
					// If repoDir is found, return nil
					if path == repoDir { return nil }
					// If file is directory
					if info.IsDir() {
						// Decode toml and append to map (secondary err var due to arg of function)
						_, err2 := toml.DecodeFile(path + "/repolist.toml", &repoListData)
						if err2 != nil { return err }
					}
					// Return nil if nothing happens
					return nil
				})
				if err != nil { log.Warn().Err(err).Msg("Error walking repo directory") }
				//fmt.Println(repoListData) //DEBUG

				// Create cloneURL variable for storage of URL to be cloned
				var cloneURL string
				// If it exists in repolist, put repo of package into cloneURL
				if repoListData[e] != nil {
					cloneURL = repoListData[e].(string)
				} else {
					// Warn user if package nonexistent in repolist
					log.Warn().Msg(`Package "` + e + `" not found in repolists`)
					continue
				}
				// Clone repo from repolist
				_, cloneDir = GitClone(cloneURL, dlDir)
			}

			// Log config read
			log.Info().Msg("Reading config")
			//Create map[string]interface{} for unstructured TOML decode
			var result map[string]interface{}
			// Decode the TOML config into the result variable
			toml.DecodeFile(cloneDir + "/.acorn/meta.toml", &result)
			// Determine package manager installed on user's system
			pkgMan := determinePackageManager()
			// Get dependencies for package manager specified
			deps := result["dependencies"].(map[string]interface{})[pkgMan].([]interface{})
			// Run install command for previously determined package manager
			if len(deps) != 0 { packageManagerInstall(pkgMan, deps) }
			//fmt.Println(result) //DEBUG
			// Get build script location from meta.toml
			buildScript := result["scripts"].(map[string]interface{})["build"]
			// Get install script location from meta.toml
			installScript := result["scripts"].(map[string]interface{})["install"]
			log.Info().Msg("Running build script")
			// Run build script without root privileges if it exists
			if buildScript != nil { runScript(cloneDir, buildScript.(string)) } else {
				log.Info().Msg("No build script specified, skipping")
			}
			log.Info().Msg("Running install script")
			// Run install script with root privileges if it exists
			if installScript != nil { runScriptWithSudo(cloneDir, installScript.(string)) } else {
				log.Info().Msg("No install script specified, skipping")
			}
			log.Info().Msg(`Package "` + result["name"].(string) + `" installed`)
		}
	case "remove", "rm":
		// Print usage and exit if no package is provided
		if len(args) == 1 {
			log.Error().Msg("Package not provided")
			removeUsage()
			os.Exit(1)
		}
		for _, e := range args[1:] {
			// Log config read
			log.Info().Msg("Reading config")
			//Create map[string]interface{} for unstructured TOML decode
			var result map[string]interface{}
			// Decode the TOML config into the result variable
			//fmt.Println(dlDir + e + "/.acorn/meta.toml") //DEBUG
			toml.DecodeFile(dlDir + e + "/.acorn/meta.toml", &result)
			// Determine package manager installed on user's system
			pkgMan := determinePackageManager()
			// Get dependencies for package manager specified
			deps := result["dependencies"].(map[string]interface{})[pkgMan].([]interface{})
			// If there are dependencies, remove them
			if len(deps) != 0 { packageManagerRemove(pkgMan, deps) }
			removeScript := result["scripts"].(map[string]interface{})["remove"]
			log.Info().Msg("Running remove script")
			if removeScript != nil { runScriptWithSudo(dlDir + e, removeScript.(string)) } else {
				log.Info().Msg("No remove script specified, skipping")
			}
			log.Info().Msg("Removing git repository")
			os.RemoveAll(dlDir + e)
			log.Info().Msg(`Package "` + result["name"].(string) + `" removed`)
		}
	case "update", "up":
		if len(args) == 1 {
			log.Error().Msg("Package not provided")
			updateUsage()
			os.Exit(1)
		}
		for _, e := range args[1:] {
			log.Info().Msg("Opening git repo")
			repo, err := git.PlainOpen(dlDir + e)
			if err != nil { log.Fatal().Msg(`Package "` + e + `" is not installed`) }
			worktree, err := repo.Worktree()
			if err != nil { log.Fatal().Err(err).Msg("Error getting worktree from repo") }
			log.Info().Msg("Pulling changes from git repo")
			err = worktree.Pull(&git.PullOptions{RemoteName: "origin", Force: true})
			if err != nil { log.Warn().Err(err).Msg("Error pulling from origin") }
			// Log config read
			log.Info().Msg("Reading config")
			//Create map[string]interface{} for unstructured TOML decode
			var result map[string]interface{}
			// Decode the TOML config into the result variable
			toml.DecodeFile(dlDir + e + "/.acorn/meta.toml", &result)
			pkgMan := determinePackageManager()
			deps := result["dependencies"].(map[string]interface{})[pkgMan].([]interface{})
			if len(deps) != 0 { packageManagerInstall(pkgMan, deps) }
			//fmt.Println(result) //DEBUG
			// Get build script location from meta.toml
			buildScript := result["scripts"].(map[string]interface{})["build"]
			// Get install script location from meta.toml
			installScript := result["scripts"].(map[string]interface{})["install"]
			log.Info().Msg("Running build script")
			// Run build script without root privileges if it exists
			if buildScript != nil { runScript(dlDir + e, buildScript.(string)) } else {
				log.Info().Msg("No build script specified, skipping")
			}
			log.Info().Msg("Running install script")
			// Run install script with root privileges if it exists
			if installScript != nil { runScriptWithSudo(dlDir + e, installScript.(string)) } else {
				log.Info().Msg("No install script specified, skipping")
			}
			log.Info().Msg(`Package "` + result["name"].(string) + `" updated`)
		}
	case "help":
		if len(args) == 1 {
			mainUsage()
		} else {
			switch args[1] {
			case "install", "in":
				installUsage()
			case "remove", "rm":
				removeUsage()
			case "update", "up":
				updateUsage()
			case "show", "sh":
				showUsage()
			case "list", "ls":
				listUsage()
			case "checkout", "co":
				checkoutUsage()
			case "addlist", "al":
				addlistUsage()
			case "rmlist", "rl":
				rmlistUsage()
			case "short":
				shortUsage()
			default:
				log.Fatal().Msg(`Help not available for "` + args[1] + `" command` )
			}
		}
	case "show", "sh":
		if len(args) == 1 {
			log.Error().Msg("Package not provided")
			showUsage()
			os.Exit(1)
		}
		for _, e := range args[1:] {
			// Log config read
			log.Info().Msg("Reading config")
			//Create map[string]interface{} for unstructured TOML decode
			var result map[string]interface{}
			// Decode the TOML config into the result variable
			_, err := toml.DecodeFile(dlDir + e + "/.acorn/meta.toml", &result)
			if err != nil { log.Fatal().Err(err).Msg("Error reading TOML file") }

			fileStats, err := os.Stat(dlDir + e)

			name := result["name"].(string)
			description := result["desc"]

			fmt.Println("Package:", Magenta(name))
			if description != nil { fmt.Println("Description:", Magenta(description.(string))) }
			fmt.Println("Last Update:", Magenta(fileStats.ModTime()))
			size, _ := DirSize(dlDir + e)
			sizeHumanReadable := size / 1024.0 / 1024.0
			sizeSuffix := "MB"
			if sizeHumanReadable > 1024 {
				sizeHumanReadable = sizeHumanReadable / 1024.0
				sizeSuffix = "GB"
			}
			fmt.Println("Size:", Magenta(strconv.Itoa(int(sizeHumanReadable)) + sizeSuffix))
		}
	case "checkout", "co":
		if len(args) == 1 {
			log.Error().Msg("Package not provided")
			updateUsage()
			os.Exit(1)
		} else if len(args) == 2 {
			log.Error().Msg("Commit not provided")
			updateUsage()
			os.Exit(1)
		}
		dir, commit := dlDir + args[1], args[2]
		repo, err := git.PlainOpen(dir)
		if err != nil { log.Fatal().Msg(`Package ""` + args[1] + `" is not installed`) }
		worktree, err := repo.Worktree()
		if err != nil { log.Fatal().Err(err).Msg("Error getting worktree from repo") }
		log.Info().Msg(`Checking out "` + args[2] +`"`)
		err = worktree.Checkout(&git.CheckoutOptions{
			Hash: plumbing.NewHash(commit),
			Force: true,
		})
		if err != nil { log.Fatal().Err(err).Msg("Error during checkout") }
		// Log config read
		log.Info().Msg("Reading config")
		//Create map[string]interface{} for unstructured TOML decode
		var result map[string]interface{}
		// Decode the TOML config into the result variable
		toml.DecodeFile(dir + "/.acorn/meta.toml", &result)
		// Determine package manager installed on user's system
		pkgMan := determinePackageManager()
		// Get dependencies for package manager specified
		deps := result["dependencies"].(map[string]interface{})[pkgMan].([]interface{})
		// Run install command for previously determined package manager
		if len(deps) != 0 { packageManagerInstall(pkgMan, deps) }
		//fmt.Println(result) //DEBUG
		// Get build script location from meta.toml
		buildScript := result["scripts"].(map[string]interface{})["build"]
		// Get install script location from meta.toml
		installScript := result["scripts"].(map[string]interface{})["install"]
		log.Info().Msg("Running build script")
		// Run build script without root privileges if it exists
		if buildScript != nil { runScript(dir, buildScript.(string)) } else {
			log.Info().Msg("No build script specified, skipping")
		}
		log.Info().Msg("Running install script")
		// Run install script with root privileges if it exists
		if installScript != nil { runScriptWithSudo(dir, installScript.(string)) } else {
			log.Info().Msg("No install script specified, skipping")
		}
		log.Info().Msg(`Package "` + result["name"].(string) + `" switched to ` + commit)

	case "list", "ls":
		files, err := ioutil.ReadDir(dlDir)
		if err != nil { log.Err(err).Msg("Error getting directory listing") }
		fmt.Println("Installed packages:")
		for _, file := range files {
			fmt.Println(Magenta(file.Name()))
		}
	case "addlist", "al":
		if len(args) == 1 {
			log.Error().Msg("Repolist not provided")
			addlistUsage()
			os.Exit(1)
		}
		// Add specified repolists
		for _, e := range args[1:] {
			// Log add repolist
			log.Info().Msg("Adding repolist  " + e)
			// Clone repo specified to repoDir
			_, cloneDir := GitClone(e, repoDir)
			// If the cloned repo does not contain repolist.toml, remove it
			if _, err := os.Stat(cloneDir + "/repolist.toml"); err != nil {
				log.Warn().Msg(e + " does not contain repolist.toml, removing")
				// Remove cloned repolist
				err = os.RemoveAll(cloneDir)
				if err != nil {log.Fatal().Err(err).Msg("Error removing invalid repolist")}
			}
		}
	case "rmlist", "rl":
		if len(args) == 1 {
			log.Error().Msg("Repolist not provided")
			rmlistUsage()
			os.Exit(1)
		}
		err := filepath.Walk(repoDir, func(path string, info os.FileInfo, err error) error {
			if err != nil {return err}
			if Contains(args[1:], info.Name()) {
				err2 := os.RemoveAll(path)
				if err2 != nil {return err2}
			}
			return nil
		})
		log.Fatal().Err(err).Msg("Error removing repolist")
	default:
		log.Fatal().Msg("Unknown Command: " + args[0])
	}
}