package main

import (
	"github.com/go-git/go-git/v5"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
	"strings"
)

func GitClone(url, dir string) (*git.Repository, string) {
	// Use ConsoleWriter logger
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	dirArr := strings.Split(url, "/")
	repoName := strings.Split(dirArr[len(dirArr) - 1], ".")[0]
	repo, err := git.PlainClone(dir + repoName, false, &git.CloneOptions{
		URL:      url,
		Progress: os.Stdout,
	})
	if err != nil {
		if err == git.ErrRepositoryAlreadyExists {
			log.Fatal().Msg(url + " is already installed")
			repo, _ := git.PlainOpen(dir + repoName)
			return repo, dir + repoName
		} else {
			log.Fatal().Err(err).Msg("Error cloning repo")
		}
	}
	return repo, dir + repoName
}
