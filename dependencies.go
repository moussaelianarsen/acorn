package main

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
	"os/exec"
	"runtime"
	"strings"
)

func determinePackageManager() string {
	// Use ConsoleWriter logger
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	packageManager := ""
	if strings.Contains(runtime.GOOS, "darwin") {
		if _, err := exec.LookPath("brew"); err != nil {
			log.Fatal().Msg("You must install homebrew on macOS")
		} else {
			packageManager = "brew"
		}
	} else if strings.Contains(runtime.GOOS, "linux") {
		pkgManagers := []string{"apt", "dnf", "yum", "zypper", "yay", "pacman"}
		for _, pkgManager := range pkgManagers {
			if _, err := exec.LookPath(pkgManager); err != nil {
				continue
			} else {
				packageManager = pkgManager
			}
		}
		if packageManager == "" {
			log.Fatal().Str("supported", strings.Join(pkgManagers, ", ")).Msg("No supported package manager found")
		}
	}
	return packageManager
}

func packageManagerInstall(pkgMan string, dependencies []interface{}) {
	// Use ConsoleWriter logger
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	var args []string
	if pkgMan == "pacman" {
		args = []string{"sudo", pkgMan, "-Rss"}
	} else if pkgMan == "yay" {
		args = []string{pkgMan, "-Rss"}
	} else {
		args = []string{"sudo", pkgMan, "install"}
	}
	for _, e := range dependencies {
		args = append(args, e.(string))
	}
	shExecPath, _ := exec.LookPath("sh")

	//fmt.Println(shExecPath, "-c", args) //DBBUG
	pkgManCmd := exec.Cmd{
		Path: shExecPath,
		// Use shell to run sudo with package manager to install dependencies
		Args: []string{shExecPath, "-c", strings.Join(args, " ")},
		Stdout: os.Stdout,
		Stderr: os.Stderr,
		Stdin: os.Stdin,
	}
	err := pkgManCmd.Run()
	if err != nil { log.Fatal().Err(err).Msg("Error installing dependencies") }
}

func packageManagerRemove(pkgMan string, dependencies []interface{}) {
	// Use ConsoleWriter logger
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	var args []string
	if pkgMan == "pacman" {
		args = []string{"sudo", pkgMan, "-Rss"}
	} else if pkgMan == "yay" {
		args = []string{pkgMan, "-Rss"}
	} else {
		args = []string{"sudo", pkgMan, "remove"}
	}
	for _, e := range dependencies {
		args = append(args, e.(string))
	}
	shExecPath, _ := exec.LookPath("sh")

	//fmt.Println(shExecPath, "-c", args) //DEBUG

	pkgManCmd := exec.Cmd{
		Path: shExecPath,
		// Use shell to run sudo with package manager to install dependencies
		Args: []string{shExecPath, "-c", strings.Join(args, " ")},
		Stdout: os.Stdout,
		Stderr: os.Stderr,
		Stdin: os.Stdin,
	}
	err := pkgManCmd.Run()
	if err != nil && err.Error() != "exit status 4" { log.Fatal().Err(err).Msg("Error removing dependencies") }
}