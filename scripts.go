package main

import (
	"os"
	"os/exec"
	"strings"
)

func runScript(repoDir string, scptName string) {

	args := []string{repoDir + "/.acorn/" + scptName}

	shExecPath, _ := exec.LookPath("sh")

	//fmt.Println(shExecPath, args) //DEBUG

	scptCmd := exec.Cmd{
		Path: shExecPath,
		Dir: repoDir,
		// Use shell to run sudo with package manager to install dependencies
		Args: []string{shExecPath, strings.Join(args, " ")},
		Stdout: os.Stdout,
		Stderr: os.Stderr,
		Stdin: os.Stdin,
	}
	scptCmd.Run()
}

func runScriptWithSudo(repoDir string, scptName string) {

	args := []string{repoDir + "/.acorn/" + scptName}

	sudoExecPath, _ := exec.LookPath("sudo")

	//fmt.Println(sudoExecPath, args) //DEBUG

	scptCmd := exec.Cmd{
		Path: sudoExecPath,
		Dir: repoDir,
		// Use shell to run sudo with package manager to install dependencies
		Args: []string{sudoExecPath, "--preserve-env=USER","sh", strings.Join(args, " ")},
		Stdout: os.Stdout,
		Stderr: os.Stderr,
		Stdin: os.Stdin,
	}
	scptCmd.Run()
}
