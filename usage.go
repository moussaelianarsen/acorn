package main

import (
	. "github.com/logrusorgru/aurora"
	"fmt"
)

func mainUsage() {
	fmt.Println("Acorn: Package Manager for user repositories")
	fmt.Println("Usage:", Cyan("acorn"), Magenta("<command>"), Green("[options]"))
	fmt.Println(Blue("Commands:"))
	fmt.Println(Magenta("   install"), Green("Install package"))
	fmt.Println(Magenta("    remove"), Green("Uninstall package"))
	fmt.Println(Magenta("    update"), Green("Update package"))
	fmt.Println(Magenta("      help"), Green("Show this help screen."))
	fmt.Println("          ", Green(`Use "acorn help <command>" for help with a specific command`))
	fmt.Println("          ", Green(`Use "acorn help short to see shortened commands`))
	fmt.Println(Magenta("      show"), Green("Show show info about package"))
	fmt.Println(Magenta("  checkout"), Green("Switch package to specific branch or commit"))
	fmt.Println(Magenta("   addlist"), Green("Add a repolist"))
	fmt.Println(Magenta("    rmlist"), Green("Remove a repolist"))
	fmt.Println(Magenta("      list"), Green("Show all installed packages"))
}

func shortUsage() {
	fmt.Println("Acorn: Package Manager for user repositories")
	fmt.Println("Usage:", Cyan("acorn"), Magenta("<command>"), Green("[options]"))
	fmt.Println(Blue("Commands:"))
	fmt.Println(Magenta(" in"), Green("Install package"))
	fmt.Println(Magenta(" rm"), Green("Uninstall package"))
	fmt.Println(Magenta(" up"), Green("Update package"))
	fmt.Println(Magenta(" sh"), Green("Show show info about package"))
	fmt.Println(Magenta(" co"), Green("Switch package to specific branch or commit"))
	fmt.Println(Magenta(" al"), Green("Add a repolist"))
	fmt.Println(Magenta(" rl"), Green("Remove a repolist"))
	fmt.Println(Magenta(" ls"), Green("Show all installed packages"))
}

func installUsage() {
	fmt.Println("Usage:", Cyan("acorn"), Magenta("install | in"), Green("<packageURLs>[==tag || @branch]"))
}


func removeUsage() {
	fmt.Println("Usage:", Cyan("acorn"), Magenta("remove | rm"), Green("<packageNames>"))
}

func updateUsage() {
	fmt.Println("Usage:", Cyan("acorn"), Magenta("update | up"), Green("<packageNames>"))
}

func showUsage() {
	fmt.Println("Usage:", Cyan("acorn"), Magenta("show | sh"), Green("<packageNames>"))
}

func listUsage() {
	fmt.Println("Usage:", Cyan("acorn"), Magenta("list | ls"))
}

func checkoutUsage() {
	fmt.Println("Usage:", Cyan("acorn"), Magenta("checkout | co"), Green("<packageName> <branch|commit>"))
	fmt.Println("Examples:")
	fmt.Println(Cyan("acorn"), Magenta("checkout"), Green("example master"))
	fmt.Println(Cyan("acorn"), Magenta("checkout"), Green("example 2398706a3e0b380ac1ad365488738f39917b2487"))
}

func addlistUsage() {
	fmt.Println("Usage", Cyan("acorn"), Magenta("addlist | al"), Green("<repolists>"))
}

func rmlistUsage() {
	fmt.Println("Usage", Cyan("acorn"), Magenta("rmlist | rl"), Green("<repolists>"))
}