# Acorn

## Package Manager for user repositories
[![Build status](https://ci.appveyor.com/api/projects/status/6o04yr8knelqrfe2?svg=true)](https://ci.appveyor.com/project/moussaelianarsen/acorn)
[![Download Binary](https://img.shields.io/static/v1.svg?label=download&message=binary&color=blue)](https://minio.arsenm.dev/minio/acorn/)

#### Installation

1. Clone this repository
2. Run `go build`
3. Run `sudo mkdir -p /etc/acorn/pkgs`
4. Run `sudo chown -R $(whoami) /etc/acorn`
5. Installation Complete

---

#### Usage

![Acorn Help Screen](https://i.imgur.com/mlXak4J.png)

###### Examples

1. `acorn get https://www.gitlab.com/kbdemu` This will install my other project, kbdemu.
2. `acorn update kbdemu` This will update the previously installed kbdemu.
3. `acorn show kbdemu` This will show information about kbdemu.
4. `acorn remove kbdemu` This will remove kbdemu.
5. `acorn list` This will show the names of all installed packages.